// BATAS INPUT WAKTU [23, 59]

const settings = {
    waitAfterSubmit : 4000, //milisecond. Tunggu loading setelah submit
    blankOnly : true, // isi yang belum disintuh / yang kegiatannya kosong
    excludeWeekend : true,
    overTime : false, // set true kalau mau input overtime setiap hari
    otCome : [19, 00],
    otGo : [23, 30],
    exclude: ['2020-5-2']  // tanggal yang tidak ingin diisi
}

const input = {
    ...settings,
    come: [9, 00],
    go: [18, 00],
    text: 'WFH: Solving Ticket dan Migrasi tanggal kocokan (Cuti COVID-19)', // Kegiatan
    specific: [                     //Input kegitan spesifik per tanggal
        {                    
            date: '2020-5-15',  
            come: [10, 00],
            go: [19, 00],
            text: 'WFH: Over Time',
            overTime : true,
            otCome : [19, 00],
            otGo : [23, 30],
        },
        {                    
            date: '2020-5-27',  
            come: [8, 00],
            go: [19, 00],
            text: 'WFH: Coding',
            overTime : false
        }
    ]
}

module.exports = input