const puppeteer = require('puppeteer');
const input = require('./input')
require('dotenv').config()

const config = {
    address: 'http://atrium.xsis.co.id/#!/',
    email: process.env.EMAIL,
    password: process.env.PASS
}

const weekend = ['Sabtu', 'Minggu']
const specifiDate = input.specific.length > 0 ? input.specific.map(item => item.date): []

const launchPage = async () => {
    let browser = await puppeteer.launch({headless: false})
    let page = await browser.newPage()
    await page.setViewport({width: 1024, height: 700})
    await page.goto(config.address, {waitUntil: 'networkidle2'})
    return {browser, page}
}

const logIn = async (page) => {
    let email = await page.waitForSelector('#email')
    let password = await page.waitForSelector('#password')
    await email.type(config.email)
    await password.type(config.password)
    await page.keyboard.press('Enter')
}

const goToTimesheetPage = async (page) => {
    await page.waitFor('body > header > div > nav > ul.nav.navbar-nav.ng-scope > li:nth-child(2) > a')
    let link = await page.$x("//a[contains(text(), 'Timesheet')]")
    if (link.length === 0) throw new Error('Timesheet link not found!')
    link[0].click()
}

const getCleanRowsData = async (page) => {
    let data = await page.$$('table > tbody > tr')
    data = data.slice(2, data.length)
    return data
}

const getRowButton = async (page, i) => {

    let rowData = await getCleanRowsData(page)

    let dropdown = await rowData[i].$$('button.btn.btn-warning.btn-xs.dropdown-toggle.ng-scope')
    let edit = await rowData[i].$$('div > ul > li.ng-scope > a')

    return {
        dropdown: dropdown.length > 0 ? dropdown[0] : undefined,
        edit: edit.length > 0 ? edit[0] : undefined
    }
}

const getRowsStatus = async (page) => {
    await page.waitFor('body > section > section > section > section > div.col-md-12')
    await page.waitFor(3000)

    let rowData = await getCleanRowsData(page)

    result = []
    for (let i = 0; i < rowData.length; i++) {
        let date = await rowData[i].$eval('td:nth-child(2)', el => el.textContent)
        let client = await rowData[i].$eval('td:nth-child(3)', el => el.textContent)
        let activity = await rowData[i].$eval('td:nth-child(11)', el => el.textContent)
        let dropdown = await rowData[i].$$('button.btn.btn-warning.btn-xs.dropdown-toggle.ng-scope')
        result.push({
            date: date,
            isTouched: client === '' ? false : true,
            activity: activity,
            available: dropdown.length > 0 ? true : false
        })
    }
    return [...result]
}

const selectTime = async (page, hit) => {
    for (let i = 0; i <= hit; i++) {
        await page.keyboard.press('ArrowDown')
    }
    await page.keyboard.press('Tab')
}

const populateOverTime = async (page, data) => {
    if(!data.overTime) return
    await selectTime(page, data.otCome[0])
    await selectTime(page, data.otCome[1])
    await selectTime(page, data.otGo[0])
    await selectTime(page, data.otGo[1])
    // await page.waitFor(3000)
}

const populateModal = async (page, i, data) => {
    await page.waitFor(input.waitAfterSubmit)

    let row = await getRowButton(page, i)
    await row.dropdown.click()
    await row.edit.click()
    await page.waitFor('.ngdialog-content')
    let radioButton = await page.$$('input.ng-pristine.ng-valid')
    if(radioButton.length === 0) throw new Error('No Masuk radio button')
    radioButton[0].click()

    await page.waitFor('.ng-isolate-scope')
    let datePicker = await page.$$('.ng-isolate-scope')
    if(datePicker.length === 0) throw new Error('No Date Picker')
    let timeSelect = await datePicker[0].$('select.ng-pristine:nth-child(1)')
    await timeSelect.click()
    await selectTime(page, data.come[0])
    await page.keyboard.press('Tab')
    await selectTime(page, data.come[1])
    await selectTime(page, data.go[0])
    await selectTime(page, data.go[1])
    await populateOverTime(page, data)

    await page.evaluate( () => document.querySelector("#kegiatan").value = '')
    let textArea = await page.$('#kegiatan')
    await textArea.type(data.text)
    let simpan = await page.$x("//button[contains(text(), 'Simpan')]")
    await page.waitFor(1000)
    simpan[0].click()
    // await page.evaluate(() => document.querySelector('div.ngdialog-close').click())
}

const isSpecificOrder = async (page, row, i) => {
    if(!specifiDate.includes(row.date)) return false
    let specificEntry = input.specific.find(item => item.date === row.date)
    await populateModal(page, i, specificEntry)
    console.log(row.date, `${specificEntry.come[0]}:${specificEntry.come[1]} - ${specificEntry.go[0]}:${specificEntry.go[1]}\t`, specificEntry.text)

    return true
}

const evaluateRows = async (page, rows) => {
    for (let i = 0; i < rows.length; i++) {
        let isPopulated = await isSpecificOrder(page, rows[i], i)
        if ( isPopulated ) continue
        if ( rows[i].isTouched && input.blankOnly ) continue // kalau sudah diisi
        if ( weekend.includes(rows[i].activity) && input.excludeWeekend ) continue // kalau weekend
        if ( !rows[i].available ) continue //kalau tombol biru atau belum bisa input
        if ( input.exclude.includes(rows[i].date) ) continue // kalau tanggal di-exclude
        
        await populateModal(page, i, input)
        console.log(rows[i].date, `${input.come[0]}:${input.come[1]} - ${input.go[0]}:${input.go[1]}\t`, input.text)
    }

}

const goToPrevMonth = async (page) => {
    await page.waitFor('table')
    let link = await page.$x("//a[contains(text(), 'Timesheet')]")
    link[0].focus()
    await page.keyboard.press('Tab')
    await page.keyboard.press('Tab')
    await page.keyboard.press('Enter')
    await page.keyboard.press('ArrowDown')
    await page.keyboard.press('Enter')
    await page.waitFor('table')
}

const main = async () => {
    let { browser, page } = await launchPage()
    try {
        await logIn(page)
        await goToTimesheetPage(page)

        let rows = await getRowsStatus(page)
        await evaluateRows(page, rows)
        
        await goToPrevMonth(page)
        
        rows = await getRowsStatus(page)
        await evaluateRows(page, rows)

    } catch (err) {
        console.error('ERROR!\n')
        console.log(err)
    }
    
    await page.waitFor(2000)
    await browser.close()
}

main()